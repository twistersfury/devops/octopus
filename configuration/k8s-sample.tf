resource "kubernetes_deployment" "sample" {
  metadata {
    name = "sample"
    namespace = data.terraform_remote_state.infrastructure.outputs.namespace
  }

  lifecycle {
    create_before_destroy = true
  }

  spec {
    selector {
      match_labels = {
        "tf.deployment" = "sample"
      }
    }

    template {
      metadata {
        labels = {
          "tf.deployment" = "sample"
        }
      }

      spec {
        container {
          image = "nginx:stable-alpine-slim"
          name = "nginx"
        }
      }
    }
  }
}

resource "kubernetes_service" "sample" {
  metadata {
    name      = "sample"
    namespace = data.terraform_remote_state.infrastructure.outputs.namespace
  }

  spec {
    port {
      port        = 80
      target_port = 80
    }

    selector = {
      "tf.deployment" = "sample"
    }
  }
}

resource "kubernetes_ingress_v1" "sample" {
  metadata {
    name      = "sample"
    namespace = data.terraform_remote_state.infrastructure.outputs.namespace
  }

  spec {
    ingress_class_name = ""

    rule {
      host = "beta.flatcar.freenas"

      http {
        path {
          backend {
            service {
              name = "sample"
              port {
                number = 80
              }
            }
          }

          path = "/"
        }
      }
    }
  }
}
