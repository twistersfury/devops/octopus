terraform {
  required_version = ">= 1.7"

  required_providers {
    octopusdeploy = {
      source = "OctopusDeployLabs/octopusdeploy"
      version = "0.14.4"
    }

    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "2.25.2"
    }
  }

  # Expected To Be Provided By `backend.conf`
  backend "s3" {}
}
