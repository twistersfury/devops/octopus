provider "octopusdeploy" {
  address = data.terraform_remote_state.infrastructure.outputs.application_url
  api_key = var.octopus_api_key
}

# Kubernetes Provider
provider "kubernetes" {
  config_path = var.kubernetes_config
}
