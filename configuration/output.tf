output "application_url" {
  value = data.terraform_remote_state.infrastructure.outputs.application_url
}
