data "terraform_remote_state" "infrastructure" {
#  backend = "local"
#  config = {
#    path = "../infrastructure/.terraform/terraform.tfstate"
#  }

  backend = "s3"

  config = {
    skip_credentials_validation = true
    skip_metadata_api_check     = true
    skip_requesting_account_id  = true
    skip_s3_checksum            = true

    bucket = var.infrastructure_state_bucket
    key = var.infrastructure_state_file
    region = var.infrastructure_state_region


    endpoints =                 { s3 = var.infrastructure_state_endpoint }

    access_key = var.infrastructure_state_access_key
    secret_key = var.infrastructure_state_secret_key
  }
}
