resource "octopusdeploy_lifecycle" "lifecycle" {
  name = "Lifecycle"

  phase {
    name = "Development"

    automatic_deployment_targets = [
      octopusdeploy_environment.development.id
    ]
  }

  phase {
    name = "Production"

    automatic_deployment_targets = [
      octopusdeploy_environment.production.id
    ]
  }
}

resource "octopusdeploy_project_group" "project_group" {
  name = "Project Group"
}

resource "octopusdeploy_project" "project" {
  name = "Hello World"
  lifecycle_id = octopusdeploy_lifecycle.lifecycle.id
  project_group_id = octopusdeploy_project_group.project_group.id
}

resource "octopusdeploy_environment" "development" {
  name = "Development"
}

resource "octopusdeploy_environment" "production" {
  name = "Production"
}

resource "octopusdeploy_deployment_process" "hello_world" {
  depends_on = [
    kubernetes_deployment.tentacle
  ]

  project_id = octopusdeploy_project.project.id

  step {
    condition = "Success"
    name = "Hello World"
    package_requirement = "LetOctopusDecide"
    start_trigger = "StartAfterPrevious"

   run_script_action {
     environments = [
       octopusdeploy_environment.development.id
     ]

     can_be_used_for_project_versioning = false
     condition = "Success"
     is_disabled = false
     is_required = true
     name = "Hello World"
     script_body = <<-EOT
        rm -rf /tmp/tf-extract
        mkdir -p /tmp/tf-extract

        PATH=$(get_octopusvariable 'Octopus.Action.Package[Package].ExtractedPath')
        POD=$(/usr/bin/kubectl get pod -l tf.deployment=sample --no-headers -o custom-columns=":metadata.name")

        echo "Path: $PATH"
        echo "Pod: $POD"

        /bin/tar cf - -C $PATH . | /usr/bin/kubectl exec -i $POD -- tar xf - -C /usr/share/nginx/html/

     EOT
     script_syntax = "Bash"
     run_on_server = true


     package {
       name = "Package"
       package_id = "vue-js-sample"

       extract_during_deployment = true
     }
   }

    target_roles = ["System Administrators"]
  }

#  step {
#    name = "Deployment Approval"
#
#    manual_intervention_action {
#      environments = [
#        octopusdeploy_environment.development.id
#      ]
#
#      instructions = "Please confirm the deployment."
#      name         = "Approval Deployment"
#    }
#  }

  step {
    condition = "Success"
    name = "Hello World - Production"
    package_requirement = "LetOctopusDecide"
    start_trigger = "StartAfterPrevious"

    run_script_action {
      environments = [
        octopusdeploy_environment.production.id
      ]

      can_be_used_for_project_versioning = false
      condition = "Success"
      is_disabled = false
      is_required = true
      name = "Hello World - Production"
      script_body = <<-EOT
        rm -rf /tmp/tf-extract
        mkdir -p /tmp/tf-extract

        PATH=$(get_octopusvariable 'Octopus.Action.Package[Package].ExtractedPath')
        POD=$(/usr/bin/kubectl get pod -l tf.deployment=sample-prod --no-headers -o custom-columns=":metadata.name")

        echo "Path: $PATH"
        echo "Pod: $POD"

        /bin/tar cf - -C $PATH . | /usr/bin/kubectl exec -i $POD -- tar xf - -C /usr/share/nginx/html/
     EOT
      script_syntax = "Bash"
      run_on_server = true

      package {
        name = "Package"
        package_id = "vue-js-sample"

        extract_during_deployment = true
      }

    }

    manual_intervention_action {
      environments = [
        octopusdeploy_environment.production.id
      ]

      instructions = "Please confirm the deployment."
      name         = "Approval Deployment"
    }

    target_roles = ["System Administrators"]
  }
}

resource "octopusdeploy_certificate" "development" {
  certificate_data = var.k8s_development_cert
  name             = "Development Cert"
  password         = "abc123"
}

resource "octopusdeploy_static_worker_pool" "worker_pool" {
  name = var.octopus_worker_pool
}

resource "octopusdeploy_kubernetes_cluster_deployment_target" "development" {
  cluster_url  = var.k8s_development_url

  environments = [
    octopusdeploy_environment.development.id
  ]

  default_worker_pool_id = octopusdeploy_static_worker_pool.worker_pool.id

  name         = "Development Cluster"

  roles        = ["System Administrators"]

  certificate_authentication {
    client_certificate = octopusdeploy_certificate.development.id
  }

  skip_tls_verification = true

  namespace = data.terraform_remote_state.infrastructure.outputs.namespace
}

resource "octopusdeploy_kubernetes_cluster_deployment_target" "production" {
  cluster_url  = var.k8s_production_url

  environments = [
    octopusdeploy_environment.production.id
  ]

  default_worker_pool_id = octopusdeploy_static_worker_pool.worker_pool.id

  name         = "Production Cluster"

  roles        = ["System Administrators"]

  certificate_authentication {
    client_certificate = octopusdeploy_certificate.production.id
  }

  skip_tls_verification = true

  namespace = data.terraform_remote_state.infrastructure.outputs.namespace
}

resource "octopusdeploy_certificate" "production" {
  certificate_data = var.k8s_production_cert
  name             = "Production Cert"
  password         = "abc123"
}
