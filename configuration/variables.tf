variable "infrastructure_state_access_key" {
  type = string
  description = "State File Access Key"
}

variable "infrastructure_state_bucket" {
  type = string
  description = "State File Bucket"
}

variable "infrastructure_state_endpoint" {
  type = string
  description = "State File S3 Endpoint"
}

variable "infrastructure_state_file" {
  type = string
  description = "State File Backend Key From Infrastructure Project"
}

variable "infrastructure_state_region" {
  type = string
  description = "State File Region"
}

variable "infrastructure_state_secret_key" {
  type = string
  description = "State File Secret Key"
}

variable "kubernetes_config" {
  type = string
  default = "~/.kube/config"
  description = "Path To Kubernetes Configuration"
}

variable "k8s_development_cert" {
  type = string
  description = "K8S Deployment Client Cert"
}

variable "k8s_development_url" {
  type = string
  description = "K8S Deployment URL - Development"
}

variable "k8s_production_cert" {
  type = string
  description = "K8S Production Client Cert"
}

variable "k8s_production_url" {
  type = string
  description = "K8S Production URL - Development"
}

variable "octopus_api_key" {
  type = string
  description = "API Key From User Profile For Applying Configurations"
}

variable "octopus_worker_pool" {
  type = string
  default = "Worker Pool"
  description = "Worker Pool Name"
}
