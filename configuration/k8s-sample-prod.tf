resource "kubernetes_deployment" "sample-prod" {
  metadata {
    name = "sample-prod"
    namespace = data.terraform_remote_state.infrastructure.outputs.namespace
  }

  lifecycle {
    create_before_destroy = true
  }

  spec {
    selector {
      match_labels = {
        "tf.deployment" = "sample-prod"
      }
    }

    template {
      metadata {
        labels = {
          "tf.deployment" = "sample-prod"
        }
      }

      spec {
        container {
          image = "nginx:stable-alpine-slim"
          name = "nginx"
        }
      }
    }
  }
}

resource "kubernetes_service" "sample-prod" {
  metadata {
    name      = "sample-prod"
    namespace = data.terraform_remote_state.infrastructure.outputs.namespace
  }

  spec {
    port {
      port        = 80
      target_port = 80
    }

    selector = {
      "tf.deployment" = "sample-prod"
    }
  }
}

resource "kubernetes_ingress_v1" "sample-prod" {
  metadata {
    name      = "sample-prod"
    namespace = data.terraform_remote_state.infrastructure.outputs.namespace
  }

  spec {
    ingress_class_name = ""

    rule {
      host = "charlie.flatcar.freenas"

      http {
        path {
          backend {
            service {
              name = "sample-prod"
              port {
                number = 80
              }
            }
          }

          path = "/"
        }
      }
    }
  }
}
