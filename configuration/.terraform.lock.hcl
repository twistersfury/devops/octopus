# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/kubernetes" {
  version = "2.25.2"
  hashes = [
    "h1:o/+UcYEaEHrQzq2kkWw2MohCK033u6vY+T6cmHd46QU=",
    "zh:044788ac936e0e8ece8f78a2e4e366ecd435ea8235388eaf2cbc8e7975d9d970",
    "zh:24f5ff01df91f51f00ee7ff39430adeb63bb2ca4ea0042e68f06d6b65808c02f",
    "zh:49984aa0aa1faa8c4f01e8faa039322f1e6fdaeab0b7e32f5c6e96edfde36a38",
    "zh:4eeceaff56bac9fc782e7e33f157fa2c7e9a47b2c3c3d12da2642c312ace73f6",
    "zh:4f49b6419345960d5af475e0200c243af4c9c140b0ee64799fe1fc9b023c49ea",
    "zh:7958414d516867a2263a978792a24843f80023fb233cf051ff4095adc9803d85",
    "zh:c633a755fc95e9ff0cd73656f052947afd85883a0987dde5198113aa48474156",
    "zh:cbfe958d119795004ce1e8001449d01c056fa2a062b51d07843d98be216337d7",
    "zh:cfb85392e18768578d4c943438897083895719be678227fd90efbe3500702a56",
    "zh:d705a661ed5da425dd236a48645bec39fe78a67d2e70e8460b720417cbf260ac",
    "zh:ddd7a01263da3793df4f3b5af65f166307eed5acf525e51e058cda59009cc856",
    "zh:f569b65999264a9416862bca5cd2a6177d94ccb0424f3a4ef424428912b9cb3c",
  ]
}

provider "registry.terraform.io/octopusdeploylabs/octopusdeploy" {
  version     = "0.14.4"
  constraints = "0.14.4"
  hashes = [
    "h1:c2D03SQ6YKOE8NnR8hnQ/ux/q9T/FmzOsvKwN0iIzzg=",
    "zh:13256e1567354e2255f748cce7089bf0a619e45d7e3d8efc8a414bfdcda29fa8",
    "zh:13f07606a8091ebd53c70e4614eeb732b35cb6684ccd868536de58516d3d6f37",
    "zh:2b1dfc0d65aa77c9ed41b9f99e0e1e793d2da1dbc3ca4db19af46b98be05c772",
    "zh:5fd610a7ccd56f1cb5b6a7886d5a4f8543baa1a76ce554052c4535d6477be96f",
    "zh:81eaa37beadab2b84a2fa6de4a28d325cbd8c44dc43657f9b12225c1f094f464",
    "zh:a45d8e12184bf8aad8913b880eda4ef56ddd47d08db68c8784279c633298738d",
    "zh:a8ac6c5c281be9429cdd64b3113cd930fd5a18f5d063040cab38c01d3f9732f1",
    "zh:b39b5e82a75ead70a3c228be8e3718c90955b825beca7eff2a28abcc7bf7e626",
    "zh:dccb84fc6afd4a3fd7d95af282d0028ef0a6b03285afde855199c920fbae6560",
    "zh:eb53a3fbfe55bf30a9d02b70684703da9f4b4124099fd111ee8f8c8810cca45c",
  ]
}
