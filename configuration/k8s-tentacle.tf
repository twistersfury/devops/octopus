resource "kubernetes_deployment" "tentacle" {
  depends_on = [
    octopusdeploy_static_worker_pool.worker_pool
  ]

  metadata {
    name = "octopus-tentacle"
    namespace = data.terraform_remote_state.infrastructure.outputs.namespace
  }

  lifecycle {
    create_before_destroy = true
  }

  spec {
    selector {
      match_labels = {
        "tf.deployment" = "tentacle"
      }
    }
#
#    strategy {
#      type = "Recreate"
#    }

    template {
      metadata {
        labels = {
          "tf.deployment" = "tentacle"
        }
      }

      spec {
        container {
#          image = "octopuslabs/tentacle:latest"
          image = "octopuslabs/tentacle-k8sworker"
          name = "tentacle"

          env {
            name = "ACCEPT_EULA"
            value = "Y"
          }

          env {
            name = "SERVER_URL"
            value = format("http://octopus-octopus-deploy.%s.svc.cluster.local", data.terraform_remote_state.infrastructure.outputs.namespace)
          }

          env {
            name = "SERVER_API_KEY"
            value = var.octopus_api_key
          }

          env {
            name = "TARGET_WORKER_POOL"
            value = octopusdeploy_static_worker_pool.worker_pool.name
          }

          env {
            name = "REGISTRATION_NAME"
            value = "K8S Worker"
          }

#          lifecycle {
#            pre_stop {
#              exec {
#                command = [
#                  "tentacle",
#                  "deregister-worker"
#                ]
#              }
#            }
#          }
        }
      }
    }
  }
}
