# Helm provider
provider "helm" {
    kubernetes {
        config_path = var.kubernetes_config
    }

    registry {
        password = var.docker_password
        url      = "https://registry-1.docker.io"
        username = var.docker_username
    }
}

# Kubernetes Provider
provider "kubernetes" {
    config_path = var.kubernetes_config
}
