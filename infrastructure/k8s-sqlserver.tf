resource "kubernetes_deployment" "sql_server" {
    metadata {
        name = "octopus-sql-server"
        namespace = kubernetes_namespace.octopus.metadata[0].name
    }

    lifecycle {
        create_before_destroy = true
    }

    spec {
        selector {
            match_labels = {
                "tf.deployment" = "mssql"
            }
        }

        strategy {
          type = "Recreate"
        }

        template {
            metadata {
                labels = {
                    "tf.deployment" = "mssql"
                }
            }

            spec {
                container {
                    image = "mcr.microsoft.com/azure-sql-edge"
                    name = "sql-server"

                    env {
                        name = "MSSQL_PID"
                        value = "Developer"
                    }

                    env {
                        name = "ACCEPT_EULA"
                        value = "Y"
                    }

                    env {
                        name = "SA_PASSWORD"
                        value = random_password.database_password.result
                    }

                    port {
                        container_port = 1433
                        name = "mssql"
                    }

                    readiness_probe {
                        tcp_socket {
                            port = "mssql"
                        }
                    }

                    volume_mount {
                        mount_path = "/var/opt/mssql"
                        name       = "mssqldb"
                    }
                }

                security_context {
                    fs_group = "10001"
                }

                volume {
                    name = "mssqldb"

                    persistent_volume_claim {
                        claim_name = kubernetes_persistent_volume_claim.sql_server.metadata[0].name
                    }
                }
            }
        }
    }
}

resource "kubernetes_persistent_volume_claim" "sql_server" {
    metadata {
        name = "octopus-mssql-data"
        namespace = kubernetes_namespace.octopus.metadata[0].name
    }

    spec {
        access_modes = [
            "ReadWriteOnce"
        ]

        resources {
            requests = {
                storage = "8G"
            }
        }

        storage_class_name = var.kubernetes_pvc_class
    }

    wait_until_bound = false
}

resource "kubernetes_service" "sql_server" {
  metadata {
    name      = "octopus"
    namespace = kubernetes_namespace.octopus.metadata[0].name
  }

  spec {
    port {
      node_port   = var.sql_node_port
      port        = kubernetes_deployment.sql_server.spec[0].template[0].spec[0].container[0].port[0].container_port
      target_port = kubernetes_deployment.sql_server.spec[0].template[0].spec[0].container[0].port[0].name
    }

    selector = {
      "tf.deployment" = kubernetes_deployment.sql_server.spec[0].template[0].metadata[0].labels["tf.deployment"]
    }

    type = "NodePort"
  }
}
