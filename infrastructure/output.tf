output "sql_password" {
  value = random_password.database_password.result
  sensitive = true
}

output "admin_password" {
  value = random_password.administrator_password.result
  sensitive = true
}

output "application_url" {
  value = format("http://%s%s", var.octopus_domain, var.octopus_path)
}

output "namespace" {
  value = kubernetes_namespace.octopus.metadata[0].name
}
