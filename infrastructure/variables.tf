variable "docker_password" {
    type = string
    description = "Docker Hub Password"
}

variable "docker_username" {
    type = string
    description = "Docker Hub Username"
}

variable "kubernetes_config" {
  type = string
  default = "~/.kube/config"
  description = "Path To Kubernetes Configuration"
}

variable "kubernetes_namespace" {
    type = string
    default = "octopus"
    description = "Namespace to use for all resources."
}

variable "kubernetes_pvc_class" {
    type = string
    default = "local-path"
    description = "Storage Class To Use For PVC"

    # storage_class_name = "do-block-storage-retain"
    # storage_class_name = "do-block-storage"
}

variable "octopus_admin_username" {
    type = string
    default = "administrator"
    description = "Default Administrator Username"
}

variable "octopus_domain" {
  type = string
  default = "octopus.localhost"
  description = "Domain For Accessing Octopus"
}

variable "octopus_helm_repository" {
  type = string
  default = "oci://registry-1.docker.io/octopusdeploy/octopusdeploy-helm"
  description = "Helm Chart Package Name"
}

variable "octopus_image_repository" {
  type = string
  default = "octopusdeploy/octopusdeploy@sha256"
}

variable "octopus_image_tag" {
  type = string
  default = "a396b7fd9173ef11deca550e88f58773644a2c6f4c1fb6d4b925232c4a76930e"
}

variable "octopus_license_key" {
  type = string
  default = ""
  description = "Octopus License Key"
}

variable "octopus_path" {
  type = string
  default = "/"
  description = "Ingress Path"
}

variable "sql_node_port" {
  type = number
  default = 30100
  description = "Node Port For SQL Access"
}
