resource "helm_release" "octopus" {
  name       = "octopus"
  chart      = var.octopus_helm_repository
  namespace  = kubernetes_namespace.octopus.metadata[0].name

  set {
    name = "octopus.acceptEula"
    value = "Y"
  }

  set {
    name = "octopus.masterKey"
    value = base64encode(random_string.master_key.result)
  }

  set {
    name  = "octopus.databaseConnectionString"

    value = format(
      "Server=tcp:%s.%s.svc.cluster.local; Initial Catalog=OctopusDeploy;Persist Security Info=False;User ID=sa;Password=%s;Encrypt=True;Connection Timeout=30;",
      kubernetes_service.sql_server.metadata[0].name,
      kubernetes_namespace.octopus.metadata[0].name,
      random_password.database_password.result
    )

    type  = "string"
  }

  set {
    name  = "octopus.licenseKeyBase64"
    value = var.octopus_license_key
  }

  set {
    name  = "octopus.username"
    value = var.octopus_admin_username
  }

  set {
    name  = "octopus.password"
    value = random_password.administrator_password.result
  }

  set {
    name = "octopus.image.repository"
    value = var.octopus_image_repository
  }

  set {
    name = "octopus.image.tag"
    value = var.octopus_image_tag
  }

  set {
    name = "octopus.ingress.enabled"
    value = true
  }

  set {
    name = "octopus.ingress.hosts.0"
    value = var.octopus_domain
  }

  set {
    name = "octopus.ingress.path"
    value = var.octopus_path
  }
}
