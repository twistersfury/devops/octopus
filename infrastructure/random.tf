resource "random_password" "administrator_password" {
    length = 32
}

resource "random_password" "database_password" {
    length = 32
}

resource "random_string" "master_key" {
  length = 16
}

