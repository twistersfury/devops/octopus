######################################################
# Core Configuration
######################################################

BIN_PATH := bin

MAKEFILE_JUSTNAME := $(firstword $(MAKEFILE_LIST))
MAKEFILE_COMPLETE := $(CURDIR)/$(MAKEFILE_JUSTNAME)
MAKE_OPTIONS := -f $(MAKEFILE_JUSTNAME)

######################################################
# Default Command
######################################################

.PHONY: default
default: octopus-apply

######################################################
# Include Terraform
######################################################
$(BIN_PATH)/terraform.mk: Makefile | $(BIN_PATH)
	$(MAKEFILE_DOWNLOAD_COMMAND) https://gitlab.com/twistersfury/utilities/-/raw/v3.0.0-alpha+19/make/terraform.mk

ifndef MAKEFILE_TERRAFORM
include $(BIN_PATH)/terraform.mk
endif

######################################################
# Include Local Overrides
######################################################
# If you don't have wget, then you can manually edit
# 	bin/overrides.mk to enable curl

$(BIN_PATH):
	mkdir -p $@

$(BIN_PATH)/overrides.mk: | $(BIN_PATH)
	echo 'MAKEFILE_OVERRIDES := true' > $@
	echo 'MAKEFILE_DOWNLOAD_COMMAND = wget -O $$@' >> $@
	echo '#MAKEFILE_DOWNLOAD_COMMAND = curl -o $$@' >> $@
	echo "Override File Created, You Will Have To Rerun The Previous Command."
	echo "By default, this script attempts to use wget. If you don't have wget, you can use curl instead."
	echo "If you want to use curl, edit the $@ file to use the curl version of MAKEFILE_DOWNLOAD_COMMAND." && exit 1

# Only Include If Not Already Included (Will Cause Errors Otherwise)
ifndef MAKEFILE_OVERRIDES
include $(BIN_PATH)/overrides.mk
endif

######################################################
# Infrastructure Configuration
######################################################

.PHONY: octopus-apply
octopus-apply: infrastructure/backend.conf
	$(MAKE) terraform-apply TERRAFORM_DIRECTORY=infrastructure TERRAFORM_INIT_OPTIONS="--backend-config=backend.conf"

.PHONY: octopus-destroy
octopus-destroy: infrastructure/backend.conf
	$(MAKE) terraform-destroy TERRAFORM_DIRECTORY=infrastructure TERRAFORM_INIT_OPTIONS="--backend-config=backend.conf"
	rm -rf infrastructure/.terraform

######################################################
# Deployment Configuration
######################################################
.PHONY: configuration-apply
configuration-apply: configuration/backend.conf
	$(MAKE) terraform-apply TERRAFORM_DIRECTORY=configuration TERRAFORM_INIT_OPTIONS="--backend-config=backend.conf"

.PHONY: configuration-destroy
configuration-destroy: configuration/backend.conf
	$(MAKE) terraform-destroy TERRAFORM_DIRECTORY=configuration TERRAFORM_INIT_OPTIONS="--backend-config=backend.conf"
	rm -rf configuration/.terraform
